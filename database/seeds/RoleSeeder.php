<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = ['admin','teacher','student'];
        foreach($roles as $role)
        {
            DB::table('roles')->insert([
                'title' => $role,          
            ]);
        }       
    }
}
