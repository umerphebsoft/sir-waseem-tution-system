<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentAcademicRecordTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('student_academic_record', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('student_id');
            $table->string('degree');
            $table->string('institute');
            $table->string('start_date');
            $table->string('end_date');
            $table->string('grade');
            $table->string('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('student_academic_record');
    }
}
