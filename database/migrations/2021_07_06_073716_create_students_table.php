<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->unsignedInteger('id')->autoIncrement();
            $table->string('first_name');
            $table->string('last_name');
            $table->string('cnic');
            $table->string('email');
            $table->string('country');
            $table->string('phone');
            $table->string('home_num');
            $table->string('student_type');
            $table->string('guardian_name');
            $table->string('guardian_phone');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
